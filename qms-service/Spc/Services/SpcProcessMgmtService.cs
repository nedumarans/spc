﻿using AutoMapper;
using ClosedXML.Excel;
using Microsoft.Data.Edm.Csdl;
using Microsoft.EntityFrameworkCore;
using Spc.DTOs;
using Spc.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spc.Services
{
    public class SpcProcessMgmtService
    {

        private spcContext _efRespository;

        private IMapper mapper;

        public SpcProcessMgmtService(spcContext efRepository,IMapper mapper)
        {
            _efRespository = efRepository;
            this.mapper = mapper;
        }

        public List<SpcProcessVariableDTO> GetSpcProcessVariables (int proccessid)
        {

            Models.SpcProcess spcProcess = _efRespository.SpcProcess.Include(x => x.SpcProcessVariable)
                .Where(x => x.Id == proccessid).First();
            List<SpcProcessVariableDTO> spcProcessVariables = new List<SpcProcessVariableDTO>();
            foreach(var spcVariable in spcProcess.SpcProcessVariable)
            {
                SpcProcessVariableDTO dto = this.mapper.Map<SpcProcessVariableDTO>(spcVariable);
                dto.IdSpcProcess = proccessid;
                spcProcessVariables.Add(dto);
            }

            return spcProcessVariables;
        }

        public List<SpcRuleDTO> GetSpcVariableRules(int spcVariableId)
        {

            Models.SpcProcessVariable  processVariable= _efRespository.SpcProcessVariable.Include(x => x.SpcVariableRule).ThenInclude(x => x.IdRuleNavigation)
                .Where(x => x.Id == spcVariableId).First();  
           
            List<SpcRuleDTO> scpRulesDTOs = new List<SpcRuleDTO>();
            foreach (var spcVariableRule in processVariable.SpcVariableRule) { 
                SpcRuleDTO spcRuleDTO = new SpcRuleDTO();
                spcRuleDTO.SpcProcessVariableId = spcVariableId;
                spcRuleDTO.Id = spcVariableRule.Id;
                spcRuleDTO.IsSelected = spcVariableRule.IsSelected;
                spcRuleDTO.Rule = spcVariableRule.IdRuleNavigation.Rule;
                spcRuleDTO.Description = spcVariableRule.IdRuleNavigation.Description;
                spcRuleDTO.RuleId = spcVariableRule.IdRuleNavigation.Id;
                scpRulesDTOs.Add(spcRuleDTO);
            }
            return scpRulesDTOs;
        }

        public List<SpcRuleDTO> UpdateSpcVariableRules(int spcVariableId,List<SpcRuleDTO> spcRuleDTOs)
        {

            Models.SpcProcessVariable processVariable = _efRespository.SpcProcessVariable.Include(x => x.SpcVariableRule).ThenInclude(x => x.IdRuleNavigation)
                .Where(x => x.Id == spcVariableId).First();

           // List<SpcRuleDTO> scpRulesDTOs = new List<SpcRuleDTO>();
            foreach (var spcRuleDTO in spcRuleDTOs)
            {
                SpcVariableRule spcVariableRule = processVariable.SpcVariableRule.First(x => x.Id == spcRuleDTO.Id);
                //SpcRuleDTO dto=spcRuleDTOs.First(x => x.Id == spcVariableRule.Id);               
                spcVariableRule.IsSelected = spcRuleDTO.IsSelected;
                _efRespository.Entry(spcVariableRule).State = EntityState.Modified;
                
            }
            _efRespository.Update(processVariable);           
            _efRespository.SaveChanges();
            return spcRuleDTOs;
        }

        public SpcProcessVariableDTO AddSpcProcessVariable(int processId,SpcProcessVariableDTO spcProcessVariableDTO)
        {

            SpcProcessVariable spcProcessVariable = mapper.Map<SpcProcessVariable>(spcProcessVariableDTO);
            spcProcessVariable.IdSpcProcess = processId;
            List<Models.SpcRule> SpcRules = _efRespository.SpcRule.ToList();
            List<SpcVariableRule> spcVariableRules = new List<SpcVariableRule>();
            foreach(var spcRule in SpcRules){           
                SpcVariableRule spcVariableRule = new SpcVariableRule();
                spcVariableRule.IdRule = spcVariableRule.IdRule;
                spcVariableRule.IsSelected = true;
                spcVariableRule.IdProcessVariableNavigation = spcProcessVariable;
                spcVariableRule.IdRuleNavigation = spcRule;
                spcVariableRules.Add(spcVariableRule);
            }
            spcProcessVariable.SpcVariableRule = spcVariableRules;
            _efRespository.SpcProcessVariable.Add(spcProcessVariable);
            _efRespository.SaveChanges();
            spcProcessVariableDTO.IdSpcProcess = processId;
            spcProcessVariableDTO.Id = spcProcessVariable.Id;
            return spcProcessVariableDTO;
        }

        public SpcProcessVariableDTO UpdateSpcProcessVariable(int varaiableId, SpcProcessVariableDTO spcProcessVariableDTO)
        {

            //SpcProcessVariable spcProcessVariable = mapper.Map<SpcProcessVariable>(spcProcessVariableDTO);

            Models.SpcProcessVariable processVariable = _efRespository.SpcProcessVariable.Where(x => x.Id == varaiableId).First();

            processVariable.Variable = spcProcessVariableDTO.Variable;
            processVariable.Std = spcProcessVariableDTO.Std;
            processVariable.Tolerance = spcProcessVariableDTO.Tolerance;
            processVariable.Uom = spcProcessVariableDTO.Uom;
            _efRespository.Update(processVariable);
            _efRespository.SaveChanges();
            return spcProcessVariableDTO;
        }

        public VariableMessageMapDTO  AddMessageMapForVariable(int variableId,VariableMessageMapDTO messageMapDTO)
        {
            SpcVariableMessageMap variableMessageMap = mapper.Map<SpcVariableMessageMap>(messageMapDTO);
            variableMessageMap.IdSpcProcessVariable = variableId;                     
            _efRespository.Add(variableMessageMap);
            _efRespository.SaveChanges();
            messageMapDTO.IdSpcProcessVariable = variableId;         
            return messageMapDTO;

        }

        public List<VariableMessageMapDTO> GetVariableMessageMaps()
        {
            List<SpcVariableMessageMap> spcVariableMessageMaps=_efRespository.SpcVariableMessageMap.ToList();
            List<VariableMessageMapDTO> messageMaps = new List<VariableMessageMapDTO>();
            VariableMessageMapDTO messageMapDto = null;
            if(spcVariableMessageMaps!=null && spcVariableMessageMaps.Count() > 0)
            {
                foreach (var spcVariableMessageMap in spcVariableMessageMaps){
                    messageMapDto= mapper.Map<VariableMessageMapDTO>(spcVariableMessageMap);
                    messageMaps.Add(messageMapDto);
                }
            }
            return messageMaps;
        }

        public SpcProcessDTO AddSpcProcess(SpcProcessDTO spcProcessDTO)
        {
            SpcProcess spcProcess=mapper.Map<SpcProcess>(spcProcessDTO);
            _efRespository.SpcProcess.Add(spcProcess);
            _efRespository.SaveChanges();
            spcProcessDTO.Id = spcProcess.Id;
            return spcProcessDTO;
        }

        public SpcProcessDTO GetSpcProcess(int processId)
        {
            
            var spcProcess =  _efRespository.SpcProcess.Find(processId);
            return  this.mapper.Map<SpcProcessDTO>(spcProcess);
          
        }

        public List<SpcProcessDTO> GetSpcProcesses()
        {

            var spcProcesses = _efRespository.SpcProcess.ToList();
            var spcProcessDTOs = new List<SpcProcessDTO>();
            foreach(var spcProcess in spcProcesses)
            {
                var spcProcessDTO= this.mapper.Map<SpcProcessDTO>(spcProcess);
                spcProcessDTOs.Add(spcProcessDTO);
            }
            return spcProcessDTOs;

        }

        public XLWorkbook GetDownloadTemplate(SpcProcessDTO spcProcessDTO)
        {
            
            //spcProcessMgtService.GetSpcProcessVariables
            using (var workbook = new XLWorkbook())
            {
               // List<string> processKeys = this.GetSpcProcessKeys();
               
                var worksheet = workbook.Worksheets.Add(spcProcessDTO.Material+"_"+spcProcessDTO.Process+"_"+spcProcessDTO.Machine);
                var currentRow = 1;
                worksheet.Cell(currentRow, 1).Value = spcProcessDTO.Machine;
                worksheet.Cell(currentRow, 2).Value = spcProcessDTO.Material;
                worksheet.Cell(currentRow, 3).Value = spcProcessDTO.Process;

                currentRow = 2;
                int col = 1;
                foreach(string tag in spcProcessDTO.ProcessTags)
                {
                    worksheet.Cell(currentRow,col).Value = tag;
                    col = col + 1;
                }

                currentRow = 4;
                List<SpcProcessVariableDTO> variableDTOs=this.GetSpcProcessVariables(spcProcessDTO.Id);
                col = 1;
                foreach(var variableDTO in variableDTOs){
                    worksheet.Cell(currentRow, col).Value = variableDTO.Variable;
                    col = col + 1;
                }

                return workbook;
            }
        }

        private List<string> GetSpcProcessKeys()
        {
            List<string> processKeys = new List<string>();
            processKeys.Add("Machine");
            processKeys.Add("Process");
            processKeys.Add("Material");
            return processKeys;
        }



    }
}
 