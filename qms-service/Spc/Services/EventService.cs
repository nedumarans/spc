﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Spc.DTOs;
using Spc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace Spc.Services
{
    public class EventService : TomaxBaseService
    {
        private static int LIMIT = 100;
        private static string VALUE = "value";
        private static string ROW_NUMBER = "rownumber";

        public EventService(spcContext efRepository, IMapper mapper) : base(efRepository, mapper)
        {
        }


        public ControlChartDataDTO FetchChanges(int variableId, long lastEventNumber,long lastCount)
        {
            ControlChartDataDTO controlChartDTO = GetVariableLevelData(variableId);

            XYData events = new XYData();
            IEnumerable<EventLog> eventLogs =
              _efRespository.EventLog.Where(x => x.IdVariable == variableId && x.Eventnumber > lastEventNumber).ToList().OrderByDescending(x => x.Eventnumber).Take(LIMIT).OrderBy(x => x.Eventnumber);

            return GetControlChartData(variableId, eventLogs, lastCount);

        }




        public ControlChartDataDTO GetControlChartData(int variableId)
        {
            ControlChartDataDTO controlChartDTO=GetVariableLevelData(variableId);

            XYData events = new XYData();
            IEnumerable<EventLog> eventLogs= 
              _efRespository.EventLog.Where(x => x.IdVariable == variableId).ToList().OrderByDescending(x=>x.Eventnumber).Take(LIMIT).OrderBy(x=>x.Eventnumber);

            return GetControlChartData(variableId,eventLogs, 0);

        }


        private ControlChartDataDTO GetControlChartData(int variableId, IEnumerable<EventLog> eventLogs, long lastX)
        {
            XYData events = new XYData();
            ControlChartDataDTO controlChartDTO = GetVariableLevelData(variableId);
            double[] x = new double[eventLogs.Count()];
            double[] y = new double[eventLogs.Count()];
            long[] eventnumber = new long[eventLogs.Count()];
            Dictionary<long, Dictionary<string, double>> eventNumberRowMap = new Dictionary<long, Dictionary<string, double>>();
            for (int i = 0; i < eventLogs.Count(); i++)
            {
                EventLog log = eventLogs.ElementAt(i);
                x[i] = lastX + i;
                y[i] = log.Value.Value;
                eventnumber[i] = log.Eventnumber;
                Dictionary<string, double> alertDetailsMap = new Dictionary<string, double>();
                alertDetailsMap[ROW_NUMBER] = x[i];
                alertDetailsMap[VALUE] = log.Value.Value;
                eventNumberRowMap[log.Eventnumber] = alertDetailsMap;
            }
            if (eventLogs != null && eventLogs.Count() > 0) { 
            events.YMax = (double)eventLogs.Max(x => x.Value);
            events.YMin = (double)eventLogs.Min(x => x.Value);
            events.X = x;
            events.Y = y;
            events.XMin = lastX;
            events.XMax = lastX+eventLogs.Count();
            events.EventNumbers = eventnumber;
            controlChartDTO.Events = events;
            long minEventNumber = eventLogs.Min(x => x.Eventnumber);
            long maxEventNumber = eventLogs.Max(x => x.Eventnumber);
            events.MinEvevntNumber = minEventNumber;
            events.MaxEventNumer = maxEventNumber;
            controlChartDTO.Violations = GetViolations(variableId, eventNumberRowMap, minEventNumber, maxEventNumber);
        }
            return controlChartDTO;

        }



        private ControlChartDataDTO GetVariableLevelData(int variableId)
        {

            ControlChartDataDTO controlChartDTO = new ControlChartDataDTO();
            SpcProcessVariable processVariable = _efRespository.SpcProcessVariable.Include(x => x.SpcProcessControlContext).Where(x => x.Id == variableId).FirstOrDefault();
            controlChartDTO.IdVariable = variableId;
            controlChartDTO.Variable = processVariable.Variable;
            controlChartDTO.ControlLimits = mapper.Map<ControlLimitDTO>(processVariable.SpcProcessControlContext);
            return controlChartDTO;
        }



        private XYData GetViolations(int variableId,Dictionary<long,Dictionary<string,double>> eventNumberToRowMap,long minEventNum,long maxEventNum)
        {
            XYData violations = new XYData();
            IEnumerable<Alert> alerts =
                  _efRespository.Alert.Where(x => x.IdVariable == variableId && x.Eventnumber >= minEventNum && x.Eventnumber <= maxEventNum).ToList().OrderBy(x=>x.Eventnumber);
            double[] x = new double[alerts.Count()];
            double[] y = new double[alerts.Count()];
            long[] eventNumbers = new long[alerts.Count()];
            string[] violatedRules = new string[alerts.Count()];
            long[] ids = new long[alerts.Count()];
            for(int i = 0; i < alerts.Count(); i++)
            {
                Alert alert = alerts.ElementAt(i);
                x[i] = eventNumberToRowMap[(long)alert.Eventnumber][ROW_NUMBER];
                y[i] = eventNumberToRowMap[(long)alert.Eventnumber][VALUE];
                eventNumbers[i] =alert.Eventnumber.Value;
                violatedRules[i] = alert.ViolatedRules;
                ids[i] = alert.Id;
            }
            violations.X = x;
            violations.Y = y;
            violations.EventNumbers = eventNumbers;
            violations.ViolatedRules = violatedRules;
            violations.Ids = ids;
            return violations;
        }




        public void PopulateEvent()
        {
            Random random = new Random();
            for (int i = 0; i < 1000; i++)
            {
                EventLog log = new EventLog();
                log.IdVariable = 6;
                log.Variable = "y";
                log.Value = random.Next(39,51);
                log.Eventtime = DateTime.Now;
                log.Eventnumber = DateTime.Now.Ticks;
                _efRespository.EventLog.Add(log);
                Thread.Sleep(1);
                if(log.Value <= 40 || log.Value >= 50)
                {
                    Alert alert = new Alert();
                    alert.IdVariable = log.IdVariable;
                    alert.Parameter = log.Variable;
                    alert.ViolatedRules = "{\"rules\":[1,2,3]}";
                    alert.SourceEventTime = log.Eventtime;
                    alert.Eventnumber = log.Eventnumber;
                    alert.Alerttime = DateTime.Now;
                    _efRespository.Alert.Add(alert);
                    Console.WriteLine("alert is generated and value is" + alert.Eventnumber + log.Variable);
                }
            }
            _efRespository.SaveChanges();
        }
    }
}
