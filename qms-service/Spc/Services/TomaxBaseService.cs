﻿using AutoMapper;
using Spc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spc.Services
{
    public class TomaxBaseService
    {
        protected spcContext _efRespository;

        protected AutoMapper.IMapper mapper;

        public TomaxBaseService(spcContext efRepository, IMapper mapper)
        {
            _efRespository = efRepository;
            this.mapper = mapper;
        }
    }
}
