﻿using AutoMapper;
using Spc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spc.DTOs
{
    public class EntityMappingProfile : Profile
    {
        public EntityMappingProfile()
        {
           
            CreateMap<SpcProcessVariable, SpcProcessVariableDTO>();
            CreateMap<SpcProcessControlContext, ControlLimitDTO>();
            CreateMap<SpcProcess, SpcProcessDTO>();
            CreateMap<SpcVariableMessageMap, VariableMessageMapDTO>();
        }
    }
}
