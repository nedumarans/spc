﻿using AutoMapper;
using Spc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spc.DTOs
{
    public class DTOMappingProfile : Profile
    {
        public DTOMappingProfile()
        {
           
            CreateMap<SpcProcessDTO, SpcProcess>();
            CreateMap<SpcProcessVariableDTO, SpcProcessVariable>();
            CreateMap<VariableMessageMapDTO, SpcVariableMessageMap>();

        }
    }
}
