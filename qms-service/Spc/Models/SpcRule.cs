﻿using System;
using System.Collections.Generic;

namespace Spc.Models
{
    public partial class SpcRule
    {
        public SpcRule()
        {
            SpcVariableRule = new HashSet<SpcVariableRule>();
        }

        public int Id { get; set; }
        public string Rule { get; set; }
        public string Description { get; set; }

        public virtual ICollection<SpcVariableRule> SpcVariableRule { get; set; }
    }
}
