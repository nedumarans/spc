﻿using System;
using System.Collections.Generic;

namespace Spc.Models
{
    public partial class SpcVariableRule
    {
        public int? IdRule { get; set; }
        public int? IdProcessVariable { get; set; }
        public int Id { get; set; }

        public bool IsSelected { get; set; }

        public virtual SpcProcessVariable IdProcessVariableNavigation { get; set; }
        public virtual SpcRule IdRuleNavigation { get; set; }
    }
}
