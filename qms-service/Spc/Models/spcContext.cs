﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Spc.Models
{
    public partial class spcContext : DbContext
    {
        public spcContext()
        {
        }

        public spcContext(DbContextOptions<spcContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Alert> Alert { get; set; }
        public virtual DbSet<EventLog> EventLog { get; set; }
        public virtual DbSet<SpcProcess> SpcProcess { get; set; }
        public virtual DbSet<SpcProcessControlContext> SpcProcessControlContext { get; set; }
        public virtual DbSet<SpcProcessVariable> SpcProcessVariable { get; set; }
        public virtual DbSet<SpcRule> SpcRule { get; set; }
        public virtual DbSet<SpcVariableRule> SpcVariableRule { get; set; }

        public virtual DbSet<SpcVariableMessageMap> SpcVariableMessageMap { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Host=142.93.208.231;Database=spc;Username=postgres;Password=d3pass", x => x.UseNetTopologySuite());
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Alert>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Alerttime)
                    .HasColumnName("alerttime")
                    .HasColumnType("timestamp with time zone");

                entity.Property(e => e.Cause)
                    .HasColumnName("cause")
                    .HasMaxLength(200);

                entity.Property(e => e.Eventnumber).HasColumnName("eventnumber");

                entity.Property(e => e.IdVariable).HasColumnName("id_variable");

                entity.Property(e => e.Ignore).HasColumnName("ignore");

                entity.Property(e => e.Parameter)
                    .HasColumnName("parameter")
                    .HasMaxLength(15);

                entity.Property(e => e.PreventiveAction)
                    .HasColumnName("preventive_action")
                    .HasMaxLength(200);

                entity.Property(e => e.SourceEventTime).HasColumnName("source_event_time");

                entity.Property(e => e.Variable)
                    .HasColumnName("variable")
                    .HasMaxLength(20);

                entity.Property(e => e.ViolatedRules)
                    .HasColumnName("violated_rules")
                    .HasColumnType("jsonb");
            });

            modelBuilder.Entity<EventLog>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Eventnumber)
                    .HasColumnName("eventnumber")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Eventtime)
                    .HasColumnName("eventtime")
                    .HasColumnType("timestamp with time zone");

                entity.Property(e => e.IdVariable).HasColumnName("id_variable");

                entity.Property(e => e.Tags)
                    .HasColumnName("tags")
                    .HasColumnType("jsonb");

                entity.Property(e => e.Value).HasColumnName("value");

                entity.Property(e => e.Variable)
                    .HasColumnName("variable")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<SpcProcess>(entity =>
            {
                entity.ToTable("Spc_Process");

                entity.HasIndex(e => new { e.Material, e.Process, e.Machine })
                    .HasName("spc_process_ukey")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Machine)
                    .HasColumnName("machine")
                    .HasMaxLength(50);

                entity.Property(e => e.Material)
                    .HasColumnName("material")
                    .HasMaxLength(15);

                entity.Property(e => e.Process)
                    .HasColumnName("process")
                    .HasMaxLength(50);

                entity.Property(e => e.Tags)
                    .HasColumnName("tags")
                    .HasColumnType("json");
            });

            modelBuilder.Entity<SpcProcessControlContext>(entity =>
            {
                entity.HasKey(e => e.IdVariable)
                    .HasName("context_pk");

                entity.ToTable("Spc_Process_Control_Context");

                entity.HasIndex(e => e.IdVariable)
                    .HasName("variable_uq")
                    .IsUnique();

                entity.Property(e => e.IdVariable)
                    .HasColumnName("id_variable")
                    .ValueGeneratedNever();

                entity.Property(e => e.Count)
                    .HasColumnName("count")
                    .HasColumnType("numeric");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Lcl).HasColumnName("LCL");

                entity.Property(e => e.Mean).HasColumnName("mean");

                entity.Property(e => e.Parameter)
                    .HasColumnName("parameter")
                    .HasMaxLength(15);

                entity.Property(e => e.Sigma).HasColumnName("sigma");

                entity.Property(e => e.Ucl).HasColumnName("UCL");

                entity.Property(e => e.UserLcl).HasColumnName("userLCL");

                entity.Property(e => e.UserUcl).HasColumnName("userUCL");

                entity.Property(e => e.OneSigmaLcl).HasColumnName("one_sigma_lcl");

                entity.Property(e => e.OneSigmaUcl).HasColumnName("one_sigma_ucl");

                entity.Property(e => e.TwoSigmaUcl).HasColumnName("two_sigma_ucl");

                entity.Property(e => e.TwoSigmaLcl).HasColumnName("two_sigma_lcl");

                entity.HasOne(d => d.IdVariableNavigation)
                    .WithOne(p => p.SpcProcessControlContext)
                    .HasForeignKey<SpcProcessControlContext>(d => d.IdVariable)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("context_fk");
            });

            modelBuilder.Entity<SpcProcessVariable>(entity =>
            {
                entity.ToTable("Spc_Process_Variable");

                entity.HasIndex(e => new { e.IdSpcProcess, e.Variable })
                    .HasName("spc_process_variable_ukey")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdSpcProcess).HasColumnName("id_spc_process");

                entity.Property(e => e.Std).HasColumnName("std");

                entity.Property(e => e.Tolerance).HasColumnName("tolerance");

                entity.Property(e => e.Uom)
                    .HasColumnName("uom")
                    .HasMaxLength(10);

                entity.Property(e => e.Variable)
                    .IsRequired()
                    .HasColumnName("variable")
                    .HasMaxLength(15);

                entity.HasOne(d => d.IdSpcProcessNavigation)
                    .WithMany(p => p.SpcProcessVariable)
                    .HasForeignKey(d => d.IdSpcProcess)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("id_spc_process_var_fk");
            });

            modelBuilder.Entity<SpcRule>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(200);

                entity.Property(e => e.Rule)
                    .HasColumnName("rule")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SpcVariableMessageMap>(entity =>
            {
                entity.HasKey(e => e.IdSpcProcessVariable)
                    .HasName("spc_vairable_message_map_pk");

                entity.ToTable("Spc_Variable_Message_Map");

                entity.HasIndex(e => new { e.IdSpcProcessVariable, e.Publisher, e.Name, e.Channel, e.Subtopic })
                    .HasName("spc_variable_message_map_uk")
                    .IsUnique();

                entity.Property(e => e.IdSpcProcessVariable)
                    .HasColumnName("id_spc_process_variable")
                    .ValueGeneratedNever();

                entity.Property(e => e.Channel)
                    .HasColumnName("channel")
                    .HasMaxLength(250);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(250);

                entity.Property(e => e.Publisher)
                    .HasColumnName("publisher")
                    .HasMaxLength(250);

                entity.Property(e => e.Subtopic)
                    .HasColumnName("subtopic")
                    .HasMaxLength(250);

                entity.HasOne(d => d.IdSpcProcessVariableNavigation)
                    .WithOne(p => p.SpcVariableMessageMap)
                    .HasForeignKey<SpcVariableMessageMap>(d => d.IdSpcProcessVariable)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("message_spc_variable_id_fk");
            });

            modelBuilder.Entity<SpcVariableRule>(entity =>
            {
                entity.ToTable("Spc_Variable_Rule");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdProcessVariable).HasColumnName("id_process_variable");

                entity.Property(e => e.IdRule).HasColumnName("id_rule");

                entity.Property(e => e.IsSelected).HasColumnName("IsSelected");


                entity.HasOne(d => d.IdProcessVariableNavigation)
                    .WithMany(p => p.SpcVariableRule)
                    .HasForeignKey(d => d.IdProcessVariable)
                    .HasConstraintName("spcvariable_rule_variable_fk");

                entity.HasOne(d => d.IdRuleNavigation)
                    .WithMany(p => p.SpcVariableRule)
                    .HasForeignKey(d => d.IdRule)
                    .HasConstraintName("spcvariable_rule_rule_fk");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
