﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Spc.Models
{
    public partial class SpcProcess
    {
        public SpcProcess()
        {
            SpcProcessVariable = new HashSet<SpcProcessVariable>();
        }

        private static string TAG_NAME = "tags";

        public int Id { get; set; }
        public string Material { get; set; }
        public string Tags { get; set; }
        public string Process { get; set; }
        public string Machine { get; set; }

        [NotMapped]
        public string[] ProcessTags
        {
            get
            {
                if (this.Tags != null && this.Tags.Contains(TAG_NAME))
                {
                    Dictionary<string, string[]> jsonObject = JsonConvert.DeserializeObject<Dictionary<string, string[]>>(Tags);
                    return jsonObject[TAG_NAME];
                }
                return null;
            }

            set
            {
                if (value != null)
                {
                    string tags = JsonConvert.SerializeObject(value);
                    Tags = "{\""+TAG_NAME+"\":"+tags+"}";
                }
            }
        }
        

        public virtual ICollection<SpcProcessVariable> SpcProcessVariable { get; set; }
    }
}
