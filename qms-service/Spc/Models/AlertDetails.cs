﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spc.Models
{
    public class AlertDetails
    {
        public int RowNumber { get; set; }
        public double EventNumber { get; set; }

        public double Value { get; set; }

        public string ViolatedRules { get; set; }

    }
}
