﻿  using System;
using System.Collections.Generic;

namespace Spc.Models
{
    public partial class SpcProcessControlContext
    {
        public string Parameter { get; set; }
        public decimal? Count { get; set; }
        public double? Mean { get; set; }
        public double? Sigma { get; set; }
        public double? UserUcl { get; set; }
        public double? UserLcl { get; set; }
        public double? Ucl { get; set; }
        public double? Lcl { get; set; }
        public int IdVariable { get; set; }
        public int Id { get; set; }
        public double? OneSigmaLcl { get; set; }
        public double? OneSigmaUcl { get; set; }
        public double? TwoSigmaLcl { get; set; }
        public double? TwoSigmaUcl { get; set; }

        public virtual SpcProcessVariable IdVariableNavigation { get; set; }
    }
}
