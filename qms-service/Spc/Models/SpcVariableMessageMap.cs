﻿using System;
using System.Collections.Generic;

namespace Spc.Models
{
    public partial class SpcVariableMessageMap
    {
        public int IdSpcProcessVariable { get; set; }
        public string Publisher { get; set; }
        public string Channel { get; set; }
        public string Subtopic { get; set; }
        public string Name { get; set; }

        public virtual SpcProcessVariable IdSpcProcessVariableNavigation { get; set; }
    }
}
