﻿using System;
using System.Collections.Generic;

namespace Spc.Models2
{
    public partial class EventLog
    {
        public string Variable { get; set; }
        public double? Value { get; set; }
        public DateTime? Eventtime { get; set; }
        public long Eventnumber { get; set; }
        public string Tags { get; set; }
        public int? IdVariable { get; set; }
        public long Id { get; set; }
    }
}
