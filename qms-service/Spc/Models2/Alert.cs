﻿using System;
using System.Collections.Generic;

namespace Spc.Models2
{
    public partial class Alert
    {
        public string Parameter { get; set; }
        public DateTime? Alerttime { get; set; }
        public DateTime? SourceEventTime { get; set; }
        public long Id { get; set; }
        public long? Eventnumber { get; set; }
        public string Variable { get; set; }
        public bool? Ignore { get; set; }
        public string Cause { get; set; }
        public string PreventiveAction { get; set; }
        public int? IdVariable { get; set; }
        public string ViolatedRules { get; set; }
    }
}
