﻿using System;
using System.Collections.Generic;

namespace Spc.Models2
{
    public partial class SpcProcessVariable
    {
        public SpcProcessVariable()
        {
            SpcVariableRule = new HashSet<SpcVariableRule>();
        }

        public int Id { get; set; }
        public string Variable { get; set; }
        public int IdSpcProcess { get; set; }
        public string Uom { get; set; }
        public double? Std { get; set; }
        public double? Tolerance { get; set; }

        public virtual SpcProcess IdSpcProcessNavigation { get; set; }
        public virtual SpcProcessControlContext SpcProcessControlContext { get; set; }
        public virtual SpcVariableMessageMap SpcVariableMessageMap { get; set; }
        public virtual ICollection<SpcVariableRule> SpcVariableRule { get; set; }
    }
}
