﻿using System;
using System.Collections.Generic;

namespace Spc.Models2
{
    public partial class SpcProcess
    {
        public SpcProcess()
        {
            SpcProcessVariable = new HashSet<SpcProcessVariable>();
        }

        public int Id { get; set; }
        public string Material { get; set; }
        public string Tags { get; set; }
        public string Process { get; set; }
        public string Machine { get; set; }

        public virtual ICollection<SpcProcessVariable> SpcProcessVariable { get; set; }
    }
}
