﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using CsvHelper;
using D3Minds.Tomax.QMS.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Spc.DTOs;
using Spc.Models;

namespace Spc.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpcProcessesController : TomaxBaseController
    {
        //private readonly spcContext _context;

        public SpcProcessesController(IServiceProvider provider) :base(provider)
        {
            
        }

        // GET: api/SpcProcesses
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SpcProcessDTO>>> GetSpcProcess()
        {
            return this.spcProcessMgtService.GetSpcProcesses();
        }

        // GET: api/SpcProcesses/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SpcProcessDTO>> GetSpcProcess(int id)
        {
           return  this.spcProcessMgtService.GetSpcProcess(id);
        }

        // GET: api/SpcProcesses/5
        [HttpGet]
        [Route("api/SpcProcesses/{id}/SpcProcessVariables")]
        public  ActionResult<IEnumerable<SpcProcessVariableDTO>> GetSpcProcessVarables([FromRoute]int id)
        {

            return this.spcProcessMgtService.GetSpcProcessVariables(id);
        }

        
        [HttpPost]
        [Route("api/SpcProcesses/{id}/SpcProcessVariables")]
        public ActionResult<SpcProcessVariableDTO> AddSpcProcessVarables([FromRoute]int id,[FromBody]SpcProcessVariableDTO spcProcessVariableDTO)
        {

             return spcProcessVariableDTO = this.spcProcessMgtService.AddSpcProcessVariable(id,spcProcessVariableDTO);
            


        }

        [HttpPost]
        [Route("api/SpcProcessVariable/{spcProcessVariableId}/MessageMap")]
        public ActionResult<VariableMessageMapDTO> AddVariableMap([FromRoute]int spcProcessVariableId, [FromBody]VariableMessageMapDTO messageMapDTO)
        {

            return this.spcProcessMgtService.AddMessageMapForVariable(spcProcessVariableId, messageMapDTO);


        }

        [HttpGet]
        [Route("api/SpcVariableMessageMaps")]
        public ActionResult<List<VariableMessageMapDTO>> GetVariableMessageMap()
        {

            return this.spcProcessMgtService.GetVariableMessageMaps();


        }

        [HttpPut]
        [Route("api/SpcProcessVariables/{processVariableId}/update")]
        public ActionResult<SpcProcessVariableDTO> UpdateSpcProcessVarables([FromRoute]int processVariableId, [FromBody]SpcProcessVariableDTO spcProcessVariableDTO)
        {

            return spcProcessVariableDTO = this.spcProcessMgtService.UpdateSpcProcessVariable(processVariableId, spcProcessVariableDTO);

        }


        [HttpGet]
        [Route("api/SpcProcessVariables/{variableId}/SpcRules")]
        public ActionResult<IEnumerable<SpcRuleDTO>> GetSpcVariableRules([FromRoute]int variableId)
        {

            return this.spcProcessMgtService.GetSpcVariableRules(variableId);
        }

        [HttpPut]
        [Route("api/SpcProcessVariables/{variableId}/SpcRules")]
        public ActionResult<IEnumerable<SpcRuleDTO>> UpdateSpcVariableRules([FromRoute]int variableId,[FromBody]List<SpcRuleDTO> spcRuleDTOs)
        {

            return this.spcProcessMgtService.UpdateSpcVariableRules(variableId,spcRuleDTOs);
        }

        // PUT: api/SpcProcesses/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSpcProcess(int id, SpcProcess spcProcess)
        {
            if (id != spcProcess.Id)
            {
                return BadRequest();
            }

            _context.Entry(spcProcess).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SpcProcessExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SpcProcesses
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<SpcProcessDTO>> PostSpcProcess(SpcProcessDTO spcProcessDTO)
        {
            SpcProcessDTO spcProcessDTO1 = this.spcProcessMgtService.AddSpcProcess(spcProcessDTO);

            return CreatedAtAction("GetSpcProcess", new { id = spcProcessDTO1.Id }, spcProcessDTO1);
        }

        // DELETE: api/SpcProcesses/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<SpcProcess>> DeleteSpcProcess(int id)
        {
            var spcProcess = await _context.SpcProcess.FindAsync(id);
            if (spcProcess == null)
            {
                return NotFound();
            }

            _context.SpcProcess.Remove(spcProcess);
            await _context.SaveChangesAsync();

            return spcProcess;
        }

        private bool SpcProcessExists(int id)
        {
            return _context.SpcProcess.Any(e => e.Id == id);
        }


        [HttpGet]
        [Route("api/SpcProcesses /{id}/downloadTemplate")]
        public IActionResult DownloadTemplate([FromRoute]int id)
        { 
                using (var stream = new MemoryStream())
                {
                    var  spcProcessDTO = this.spcProcessMgtService.GetSpcProcess(id);
                    //var workbook = this.spcProcessMgtService.GetDownloadTemplate(spcProcessDTO);
                    var workbook = new XLWorkbook();
                    var worksheet = workbook.Worksheets.Add(spcProcessDTO.Material + "_" + spcProcessDTO.Process + "_" + spcProcessDTO.Machine);
                    var currentRow = 1;
                
                    worksheet.Cell(currentRow, 1).Value = spcProcessDTO.Machine;
                    worksheet.Cell(currentRow,2).Value = spcProcessDTO.Material;
                    worksheet.Cell(currentRow,3).Value = spcProcessDTO.Process;

                    int col = 1;
                    currentRow = 2;
                     worksheet.Cell(currentRow, col).Value = "Timestamp";
                   foreach (string tag in spcProcessDTO.ProcessTags)
                    {
                     col = col + 1;
                        worksheet.Cell(currentRow, col).Value = tag;
                        
                   
                    }               
                    List<SpcProcessVariableDTO> variableDTOs = this.spcProcessMgtService.GetSpcProcessVariables(spcProcessDTO.Id);             
                    foreach (var variableDTO in variableDTOs)
                    {
                        worksheet.Cell(currentRow, col).Value = variableDTO.Variable;
                        col = col + 1;
                    }
                    workbook.SaveAs(stream);
                        var content = stream.ToArray();
                        var fileName = spcProcessDTO.Material + "_" + spcProcessDTO.Process + "_" + spcProcessDTO.Material + ".xlsx";
                        return File(
                            content,
                            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName
                            );
                }
            
        }
    }
}
