﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Spc.Models;
using Spc.Services;

namespace D3Minds.Tomax.QMS.Controllers
{
    //[ServiceFilter(typeof(CustomActionFilter))]
    public class TomaxBaseController : ControllerBase
    {
        //protected readonly ICorrelationContextAccessor _correlationContext;
        protected short loggedInUserId = 1;
        //protected readonly IWebHostEnvironment _env;

        protected IServiceProvider _provider = null;
        protected IMemoryCache _cache = null;
        protected IConfiguration _configuration = null;
        protected spcContext _context = null;
        protected IMapper _mapper = null;
        protected SpcProcessMgmtService spcProcessMgtService = null;
        protected EventService eventService = null;
        public TomaxBaseController(IServiceProvider provider)
        {
            //StaticServiceProvider.Provider = provider;

            _provider = provider;

            //_env = env;

            _configuration = (IConfiguration)_provider.GetService(typeof(IConfiguration));


           // _correlationContext = (ICorrelationContextAccessor)_provider.GetService(typeof(ICorrelationContextAccessor));

            _cache = (IMemoryCache)_provider.GetService(typeof(IMemoryCache));
            _context = (spcContext)_provider.GetService(typeof(spcContext));

            //_logger = (ILoggerManager)_provider.GetService(typeof(ILoggerManager));
            _mapper = (IMapper)_provider.GetService(typeof(IMapper));
            spcProcessMgtService = (SpcProcessMgmtService)_provider.GetService(typeof(SpcProcessMgmtService));
            eventService = (EventService)_provider.GetService(typeof(EventService));
            //_appSettings = ((IOptions<Common.AppSettings>)_provider.GetService(typeof(IOptions<Common.AppSettings>))).Value;

            // _controllerHelper = (ControllerHelper)_provider.GetService(typeof(ControllerHelper));

        }

        protected void MarkDatabaseAsModified<T>(T entity) where T : class
        {
           // _controllerHelper.MarkDatabaseAsModified(entity);
        }

        protected void CommitDatabaseChanges()
        {
            //_controllerHelper.CommitDatabaseChanges();
        }

        protected bool IsAuthorized(string policy)
        {
            bool isAuthorized = true;
           /* if(true == _appSettings.EnableSecurity)
            {
                isAuthorized = false;
                var requiredRoles = _context.RolePolicy.Where(i => i.PolicyU.Name.ToLower() == policy.ToLower()).Select(i => i.RoleU.RoleName).ToList();
                foreach (var role in requiredRoles)
                {
                    isAuthorized = User.IsInRole(role);
                    if (isAuthorized) break;
                }
                return isAuthorized;
            }*/
            return isAuthorized;
        }


        protected string GetUserName()
        {
            if (User != null && User.Identity != null)
                return User.Identity.Name;

            return String.Empty;
        }
    }
}
