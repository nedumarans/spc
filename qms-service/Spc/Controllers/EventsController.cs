﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using D3Minds.Tomax.QMS.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Spc.DTOs;

namespace Spc.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : TomaxBaseController
    {
        public EventsController(IServiceProvider provider) : base(provider)
        {

        }

        [HttpGet]
        public ActionResult PopulateTestDate()
        {

            this.eventService.PopulateEvent();
            return Ok();
        }

        [HttpGet]
        [Route("SpcVariables/{id}/ControlChartData")]
        public ActionResult<ControlChartDataDTO> FetechControlChartData([FromRoute]int id)
        {

            return eventService.GetControlChartData(id);

        }


        [HttpGet]
        [Route("SpcVariables/{id}/ControlChartData/FetchChanges")]
        public ActionResult<ControlChartDataDTO> FetchChanges([FromRoute]int id, [FromQuery]long lastEventNumber,[FromQuery]long lastCount)
        {

            return eventService.FetchChanges(id,lastEventNumber,lastCount);

        }

       




    }
}