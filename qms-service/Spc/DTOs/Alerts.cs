﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spc.DTOs
{
    public class Alerts
    {
        public double[] X { get; set; }

        public double[] Y { get; set; }

        public string[] ViolatedRules { get; set; }

        public string[] Tags { get; set; }


       



    }
}
