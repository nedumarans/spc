﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spc.DTOs
{

    public class XYData
    {
        public double[] X { set; get; }

        public double[] Y { get; set; }

        public long[] Ids { get; set; }

        public long[] EventNumbers { get; set; }

        public double YMin { get; set; }

        public double YMax { get; set; }

        public double XMin { get; set; }

        public double XMax { get; set; }

        public string[] ViolatedRules { get; set; }

        public long MinEvevntNumber { get; set; }

        public long MaxEventNumer { get; set; }
    }
}
