﻿using System;
using System.Collections.Generic;

namespace Spc.DTOs
{
    public partial class SpcProcessVariableDTO
    {
        public SpcProcessVariableDTO()
        {

        }

        public int Id { get; set; }
        public string Variable { get; set; }
        public int IdSpcProcess { get; set; }
        public string Uom { get; set; }
        public double? Std { get; set; }
        public double? Tolerance { get; set; }
       



    }
}
