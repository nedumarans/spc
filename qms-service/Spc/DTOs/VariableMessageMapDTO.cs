﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spc.DTOs
{
    public class VariableMessageMapDTO
    {
        public int IdSpcProcessVariable { get; set; }
        public string Publisher { get; set; }
        public string Channel { get; set; }
        public string Subtopic { get; set; }
        public string Name { get; set; }
    }
}
