﻿using System;
using System.Collections.Generic;

namespace Spc.DTOs
{
    public partial class SpcRuleDTO
    {
        public SpcRuleDTO()
        {
            
        }

        public int SpcProcessVariableId { get; set; }
        public int Id { get; set; }
        public string Rule { get; set; }
        public string Description { get; set; }

        public int RuleId { get; set; }
        public bool IsSelected { get; set; }

    }
}
