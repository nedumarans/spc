﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Spc.DTOs
{
    public class ControlChartDataDTO
    {
        public int IdVariable { get; set; }

        public string Variable { get; set; }

        public string UOM { get; set; }

        public XYData Events { get; set; }
        public XYData Violations { get; set; }

        public ControlLimitDTO ControlLimits { get; set; }

       
      
    }



        
}
