﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spc.DTOs
{
    public class SpcProcessDTO
    {
        public int Id { get; set; }
        public string Material { get; set; }
        public string[] ProcessTags { get; set; }
        public string Process { get; set; }
        public string Machine { get; set; }

       
    }
}
